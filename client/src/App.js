import React, { useEffect, useState } from 'react';
import 'antd/dist/antd.css';
import { Card, Form, InputNumber, Button, Layout, Row, Col, Divider } from 'antd';
import Web3 from 'web3';
import SimpleStorageArtifact from './contracts/SimpleStorage.json'

function App() {
	const [ currentValue, setCurrentValue ] = useState('');

	const web3 = new Web3(Web3.givenProvider || 'http://127.0.0.1:7545');
	const simpleStorageAbi = SimpleStorageArtifact.abi;
	const simpleStorageAddress = SimpleStorageArtifact.networks[5777].address;
	const SimpleStorageContract = new web3.eth.Contract(simpleStorageAbi, simpleStorageAddress);

	const getCurrentValue = async () => {
		// Get the value from the contract.
		const response = await SimpleStorageContract.methods.get().call();

		// Update current value from the smart contract storage value.
		setCurrentValue(response);
	}

	useEffect(() => {
		getCurrentValue();
	}, [])

	const submitButtonHandler = async (values) => {
		const accounts = await window.ethereum.enable();
		// Stores input value in smart contract.
		await SimpleStorageContract.methods.set(values.storageValue).send({ from: accounts[0] });

		// Get the value from the contract to prove it worked.
		getCurrentValue();
	};

	return (
		<Layout style={{ minHeight: '100vh', padding: '16px' }} >
			<Row>
				<Col xs={24} lg={15} xl={12} xxl={9}>
					<Card title="Simple Storage" style={{ margin: '0px' }}>
						<Form
							labelCol={{
								span: 5,
							}}
							wrapperCol={{
								span: 16,
							}}
							layout="horizontal"
							size="default"
							onFinish={submitButtonHandler}
							labelAlign='left'
						>
							<Form.Item label="Current value">
								<span>{currentValue}</span>
							</Form.Item>
							<Divider></Divider>

							<Form.Item label="Value" name="storageValue" rules={[{ required: true, message: 'Please enter value!' }]} >
								<InputNumber
									min="0"
									max="999999999999999"
									style={{ width: '100%' }}
									placeholder="Enter new value"
								/>
							</Form.Item>
							<Form.Item wrapperCol={{ span: 14, offset: 5 }}>
								<Button type="primary" htmlType="submit">Save Storage Value</Button>
							</Form.Item>
						</Form>
					</Card>
				</Col>
			</Row>
		</Layout>
	);
}

export default App;
