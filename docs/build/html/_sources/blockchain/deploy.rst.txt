Deploy Smart Contract
=====================

The next task is to deploy the smart contract into a blockchain network.
To deploy a smart contract it costs ``ETH`` the cryptocurrency used in the Ethereum blockchain.
The main Ethereum blockchain network is referred to as **Mainnet**. 
To deploy a smart contract into the **Mainnet** we need real ``ETH``.
There are some test networks to test your smart contracts. 
**Ropsten**, **Kovan**, **Rinkeby** and **Goerli** are some of the test networks you can see from the Metamask.
To deploy a smart contract to one of these networks we need different ``network`` configurations in our ``truffle-config.js`` file.
In this section, we deploy our smart contracts into the Ganache blockchain which is locally 
running on our machine.
The following sub-sections will describe how to deploy our smart contract into the Ganache blockchain.

Migration file
---------------

We define the migration scripts in the ``migrations`` directory. There is an initial migration file in
``migrations`` directory ``1_initial_migration.js``.

we add a new migration file ``2_simple_storage_migration.js`` in the ``migrations`` directory for deploying our 
**SimpleStorage** smart contract to the blockchain.

In our ``2_simple_storage_migration.js`` script first we import the ``SimpleStorage`` artifact  as follows. ::

    const SimpleStorage = artifacts.require("SimpleStorage");

Then we deploy our smart contract with the constructor arguments. ``deploy`` function structured as
follows. ::

    module.exports = function(deployer) {
        deployer.deploy(<smart_contract_artifact>, <constructor_arguments, ...>);
    };

Final version of the migration file is as follows. We pass ``5`` as the initial value of the ``storedData`` attribute
of **SimpleStorage** smart contract. ::

    const SimpleStorage = artifacts.require("SimpleStorage");

    module.exports = function(deployer) {
        deployer.deploy(SimpleStorage, 5);
    };

Solc version
------------

We need to define the solidity compiler version in the ``truffle-config.js`` file.
At the bottom of the file, there is a compiler configuration section. 
We record the exact compiler version in ``solc.version`` as follows. 
We can use this version in the solidity smart contracts instead of mentioning the range of compiler versions. 
In this project we use solidity compiler version ``0.8.1`` . ::

    // Configure your compilers
    compilers: {
        solc: {
            version: "0.8.1",    // Fetch exact version from solc-bin (default: truffle's version)
            ...
        }
    },

If we do not specify the ``solc`` version it will use the truffle's default solc version.

.. _compile smart contract target:

Compile Smart Contract
----------------------

Before we deploy the smart contract we have to compile it. By running the following command we can 
compile all the smart contracts in the ``contract`` directory. 
It compiles successfully if there are no compile errors in our smart contracts. 
Run the following command on a terminal in the project root directory::

   truffle compile

.. note ::
    There are different syntaxes used in different ``solc`` versions.

If smart contracts are successfully compiled, you can see the following output.

.. image:: ../images/truffleCompile.png


It will generates a ``build`` directory in root directory. ::

    ethereum-simple-storage
    |--build
    |  |--Migrations.json
    |  |--SimpleStorage.json

The ``SimpleStorage.json`` contains the contract ABI which will be used in React app development.

Deploy Smart Contract with Truffle
----------------------------------

We can execute the following command on the terminal in the project root directory to 
deploy the **SimpleStorage** 
smart-contract into the Ganache private Etherum blockchain. 
With default settings truffle will deploy smart contracts to the Ganache, which will be running on network ``127.0.0.1`` port ``7545`` . Before executing the following command make sure that your Ganache Ethereum blockchain is running on your machine. ::

    truffle migrate --reset

We can use ``--reset`` option to run all migrations from the beginning instead of running from the 
last complete migration.

After successfully execute the above command it will print information about the deployment.
We will use the ``contract address`` of the **SimpleStorage** smart contract
and will use it in the **ReactApp**.

.. image:: ../images/truffle_migration.png

Or else, after successfully deploy the **SimpleStorage** smart contract it will update the ``SimpleStorage.json`` 
in the ``build/contracts`` directory with the deployed network and the mart contract address.
This file can be used in the **ReactApp**. 
More details about this will be discussed in the **React App Development** section.