Web Application Development Basics
==================================

In this section, we discuss Web Applications.

Web Applications are software that runs on a browser.  The browser provides user interaction with the software.  There is generally a backend server providing the necessary functionality. The backend server usually provides database support.  In our case, the backend server is a blockchain network.

Web Applications (Web Apps) are developed using web technologies like HTML, JavaScript, and CSS.
Web Apps are platform-independent, which means the same app runs on Apple MacOS, Microsoft Windows, Ubuntu, Android, or iOS.
These applications run on Internet browsers like Google Chrome, Firefox, Microsoft Edge, Brave Browser, and more.
They are used by a single or a group of users.

Websites vs Web Applications
-----------------------------

In websites, users view information provided by a company or an organization.
With a Web App, users are able to manipulate data; i.e., Create, Read, Update and Delete data (CRUD operations) of an organization.
As data is stored in the organization's database located in company premises, a Web App connects users to the organization's database over the Internet.

Generally, we do not allow users to directly manipulate the organization's database.  
There is a well defined set of business processes that are created to manipulate data.  
Users are constrained to only work with these business processes.

As such, a Web Server is placed between the Web App (Web Client) and database to incorporate the business processes and logic for user's use.
The business logic, which is implemented in the Web Server, requests data from the database, performs some processing, and serves user's request via the Web App.

Different users send different requests to the Web Server.
The Web server queries the database and serves the requests with information.
A Web App displays the requested information in a readable manner.
It not only displays the data but also facilitate users to manipulate the data.

.. image:: ../images/client-server.png

HTML, CSS, and JavaScript are basic technologies used for the development of Web Apps.  More powerful frameworks have been built  upon to facilitate such developments.

HTML
----

HTML stands for the **HyperText Markup Language**. 
HTML is the standard markup language for documents designed to be displayed in a web browser.
A HTML page consists of a series of **HTML elements**.
These elements are in the form of **HTML tags**.
There are many HTML elements that can be used for different purposes in a HTML page.

To display a paragraph in a HTML page, we  use the paragraph tag as follows: ::

    <p> This is a paragraph </p>

To display a form, we  use the following HTML elements: ::

    <form>
        <label for="value">Value: </label>
        <input type="number" id="value" name="value"><br />
        <input type="submit" value="Submit">
    </form>

There are two main parts to a HTML document: Head ``<head>`` and Body ``<body>``.
We include meta-level information in the Header portion of the HTML page.
Other components which will be displayed in the browser are placed in the Body portion.

We build a HTML page using a simple text editor and name it using the ``.html`` extension.
The following HTML page code snippet displays 10 as a current value and a form with an input filed and a submit button on the browser: ::

    <head>
        <title> HTML page </title>
    </head>
    <body>
        <form>
            <label>Current value: </label>
            <span id="currentValue" name="currentValue"> 10 </span> <br />
            <label for="value">Value: </label>
            <input type="number" id="value" name="value"><br />
            <input type="submit" value="Submit">
        </form>
    </body>

We name this HTML file ``index.html`` and save it in the computer's folder.
Subsequently, we open this file with a Google Chrome browser (use the ``Open with`` option).
The following image shows what is displayed on the Google Chrome browser:

.. image:: ../images/html.png

CSS
---

CSS stands for Cascading Style Sheets.
We add styles to a HTML page using CSS to beautify the display of the page.
We add CSS using an external CSS file, an internal CSS, or inline CSS.
The following code snnipet shows how we add inline css to a HTML page: ::

    <head>
        <title> HTML page </title>
    </head>
    <body style="background-color: #efefef; padding: 16px;">
        <div style="width: 30%; background-color: white; padding: 16px">
            <form>
                <label>Current value: </label>
                <span id="currentValue" name="currentValue"> 10 </span> <br />
                <label for="value">Value: </label>
                <input type="number" id="value" name="value"><br />
                <input type="submit" value="Submit" >
            </form>
        </div>
    </body>

The new look of the HTML page:

.. image:: ../images/css.png

CSS Framework
--------------

CSS Frameworks consist of several CSS files that are made ready-to-use for developers.
These frameworks help to develop Web Application design functions such as layouts, fonts, buttons, forms, navbars, etc.
It helps to keep the consistency of HTML components throughout the Web Application.

We use **Ant Design**, an enterprise-level UI design framework with React UI library with a set of high quality 
React Components.


JavaScript
----------

JavaScript is a scripting or programming language that allows one to implement complex features on a HTML page.
The following code snipet shows how one uses JavaScript in our ``index.html`` file to 
alert the user of the submitted value from the form and set it is as the current value: ::

    <head>
        <title> HTML page </title>
        <script>
            function submitFunction() {
                document.getElementById("currentValue").innerHTML = document.getElementById("value").value
                alert(document.getElementById("value").value)
            }
        </script>
    </head>
    <body style="background-color: #efefef; padding: 16px;">
        
        <div style="width: 30%; background-color: white; padding: 16px">
            <form onsubmit="submitFunction()">
                <label>Current value: </label>
                <span id="currentValue" name="currentValue"> 10 </span> <br />
                <br />
                <label for="value">Value: </label>
                <input type="number" id="value" name="value"><br />
                <br />
                <input type="submit" value="Submit">
            </form>
        </div>
    </body>

Unfortunately, you will not able to see the updated current value after submitting the form.  We will see how to show the current value later.

We can use JavaScript in an internal or external manner. 
In the internal manner, we use JavaScript functions inside the ``<script>`` tags within a HTML page.
In the external manner, we write JavaScript functions into a separate JavaScript file (with ``.js`` extension) and import it into the HTML page using the ``<script>`` tag.

In the following example, the pop-up window is created using an internal JavaScript function showing the submitted value from the form:

.. image:: ../images/javascript.png

JavaScript is what gives rise to user interaction in a web page.
Without JavaScript, a website will still be functional, but in a limited way.

With JavaScript, we were able to write Single Page Applications.

Single Page Applications
------------------------

A single-page application (SPA) is a Web Application that interacts with the user by 
dynamically rewriting the current web page with new data from the Web Server, 
instead of the default method of a web browser re-loading an entire page all the time.

.. image:: ../images/spa-web-server.png

In the previous example, we notice that after we submit the form, the page is re-loaded.
As a result, we will never get to see the updated value on the page.

The good thing about JavaScript is that
there are lots of JavaScript libraries developed by other people and shared with the world to fulfill the different requirements of a Web Application.  Most of the time, you just need to find and re-use what others have developed.

We make use of these libraries by importing them into our HTML code.
In this project, we are exploring how a Decentralized Application (DApp) is developed.
Our Web Application is connected to a blockchain network, instead of a Web Server.
We achieve this by using either the Web3 or EtherJS JavaScript library.

.. image:: ../images/spa-blockchain.png

JavaScript is a powerful programing language. 
Several JavaScript frameworks such as Angular and React are available for developers to write Single Page Web Applications.
Developers only need to focus on business logic and UI features, rather than speding time on navigation and complex JavaScript functions for rendering HTML components. 

React js
--------

As described in the `official site <https://reactjs.org/>`_  React is a JavaScript library for building user interfaces.
A React component is a  major concept developed in React. 
It handles the lifecycle of a UI component appearing in a Web page and its demise from a page. 
These React Components maintain  states of existence.
The UI on the page is updated whenever there are changes to a component's state. 
A component is mounted or unmounted according to user's interactions.

You can find more about React Components on `React Components page <https://reactjs.org/docs/react-component.html>`_.
Every React component returns HTML code that will be displayed on the browser.

App.js
------

There is a ``index.html`` page in the ``./public`` directory of a React application.
``index.html`` contains ``div`` HTML element and it's ``id`` is ``root``.


When React renders a Web App, it starts the rendering from ``index.js`` which is located inside the ``./src`` directory.
``index.js`` renders the ``App`` component into the ``root`` div of the ``index.html``.


The ``App`` component is defined in the ``App.js`` which is placed inside the ``./src`` directory.
``App.js`` is where we start to write our Web App functionalities. ::

    ethereum-simple-Storage
        |--client
        |  |--public
        |  |  |--index.html
        |  |--src
        |  |  |--App.js
        |  |  |--index.js

You can see how we build the ``App`` component in the next section.
