Quickstart Instructions
======================

1. Install Dependencies
-----------------------

As described in the :ref:`installation guide target` install following dependencies in your computer.

1. Install git - To check out the project from the GitLab
2. Install Node - Node will use to install other dependencies like Truffle and Node is the package manager for the React App. This project requires the Node version 14.x.
3. Install Ganache - We use Ganache to run a local blockchain network on your computer.
4. Install MetaMask - MetaMask is used for wallet account management.

2. Checkout the Project from GitLab
-----------------------------------

As described in the :ref:`checkout project target` section, 
open a terminal in your computer and execute the following command to checkout ``ethereum-simple-storage`` to your computer. ::

    git clone https://gitlab.com/gmc123/ethereum-simple-storage.git


3. Deploy Smart Contract to Ganache
------------------------------------

As described in the :ref:`deploy smart contract target` section first run Ganache on your computer.

Change directory to ``ethereum-simple-storage`` directory on your terminal and execute following command 
to deploy ``SimpleStorage`` smart contract into the Ganache local blockchain network. ::

    truffle migrate --reset

4. Copy Build Files to React App
--------------------------------

As mentioned in the :ref:`copy smart contract abi target` you must copy the ``SimpleStorage.json``into the React App.
Replace the ``SimpleStorage.json`` file in the ``client/src/contracts/`` directory from the``build/contracts/SimpleStorage.json`` file.

At this stage your project structure should be as follows. ::

    ethereum-simple-Storage
    |--build
    |  |--contracts
    |  |  |--SimpleStorage.json
    |--client
    |  |--src
    |  |  |--contracts
    |  |  |  |--SimpleStorage.json
    |--contracts
    |--docs
    |--migrations
    |--test
    |--truffle-config.js

5. Setup MetaMask
-----------------

As described in the :ref:`metamask with ganache target` section connect MetaMask to the Ganache local blockchain.

6. Install React App Dependencies
---------------------------------

In your terminal change the directory to the ``client`` directory. ::

    cd client

Then execute the following command to install React App dependencies. ::

    npm install

7. Run React App
----------------

After successfully installing React App dependencies you can execute the following command inside the ``client`` directory to run the React App. ::

    npm run start

8. Connect React App to MetaMask
--------------------------------

In your **Google Chrome** browser navigate to ``localhost:3000`` and you will be able to see the React App running on your browser.
When you load the React App on your browser MetaMask will ask to connect to React App (localhost:3000).
You can connect wallet accounts as mentioned in the :ref:`connect metamask reactapp target` section.


9. Change Value in the Simple Storage Smart Contract
----------------------------------------------------

After successfully finishing all the above steps you will be able to submit the new value using the form in the React App.
When you submit the value there will be a confirmation message popup from MetaMask to confirm the transaction.
When you confirm the transaction it will update the submitted value as the ``Current value`` in the React App.