.. Ethereum Simple Storage documentation master file, created by
   sphinx-quickstart on Wed Jul  7 11:47:33 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Simple Storage dApp Documentation
===================================================

.. image:: images/1_OohqW5DGh9CQS4hLY5FXzA.png

..

This project builds a simple Decentralized Application (dApp) using **React**, a JavaScript library for building user interfaces. 

The objectives are two fold: (1) This is like a "Hello World" kind of app where one gets hands-on experience in building the very first dApp; (2) building this dApp will bring us through various software components required for a dAPP.

This dApp deploys a smart contract holding a value
on a blockchain where users may view and modify it using a client app (a React app).


.. toctree::
   :maxdepth: 2
   :caption: Introduction

   introduction/applicationOverview

.. toctree::
   :maxdepth: 2
   :caption: Installation Guide

   installation/install
   installation/cloneproject
   installation/blockchain
   installation/wallet2blockchain
   installation/reactApp
   installation/client2wallet

.. toctree::
   :maxdepth: 2
   :caption: Smart Contract

   smartcontract/smartContract
   smartcontract/compile2deploy
   smartcontract/initNewProject

.. toctree::
   :maxdepth: 2
   :caption: Web Application Development

   webAppBasics/basics

.. toctree::
   :maxdepth: 2
   :caption: Client App

   clientApp/react
   clientApp/app
   clientApp/render

.. toctree::
   :maxdepth: 2
   :caption: Quickstart
