Application Overview
=====================

Here is a preview of the Decentralized Application (DApp) that we will first learn in this project. 
In this DApp, we are able to read and change the value of a variable in a smart contract deployed in our 
personal blockchain using the following React UI.

.. image:: ../images/app_overview.png

Architecture
------------

The following diagram shows the architecture of the project.

.. image:: ../images/architecture.png

The front-end application runs on the Google Chrome browser.  Its web page content is handled by ReactJS.  ReactJS connects to the backend blockchain through Web3.js. Any transaction to be fired to the blockchain is handled by MetaMask.  MetaMask will ask user for confirmation of transaction.

The backend consists of a local Ethereum blockchain running on Ganache.  Ganache also provides 10 accounts each loaded with 100 ether to faciliate blockchain transaction activity.
