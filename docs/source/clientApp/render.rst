Client App Rendering 🔴
===================

.. note::
    There is no code installation or command execution required in this section.  

As previously introduced, we use the following command to start the ReactApp in the ``client`` directory: ::

   npm run start 🔴

This command will start the ReactApp on network port ``3000`` in your machine.  Your browser opens up with a new page at ``localhost:3000``.  It will take some time before the client app is loaded. 

What exactly happens when this command is run?  Which file in the ``client`` directory is the entry point for running the client app?  

Whatever it is, ultimately a client app will run in your browser and something must be displayed on the web page when the client app runs. What we're gonna describe below is basic knowledge in React; there are extensive resources on the Internet and Youtube videos explaining the concepts in detail.

The files below show some of the essential files involved in the starting up of a client app. ::

    simple-storage-app
    ├── public/           # Static files
    │   └── index.html
    ├── src/              # Application root
    │   ├── contracts/
    │   ├── App.js
    │   └── index.js
    └── package.json

The essential files are ``public/index.html``, ``src/index.js`` and ``src/App.js``.  
When you create a new project, you'll always see an ``index.html`` file in the ``public`` folder. All the code you write in your ``App`` component, which acts as the `root` component, gets rendered to this HTML file. This means that there is only ONE HTML file where your code will be rendered to. Here is the body segment of the HTML file: ::

  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root"></div>
  </body>

What happens when you have a different component you would prefer to render as a different page? Do you create a new HTML file? The answer is no.  When you navigate to a new component, the ``index.html`` file will be "rewritten" to reflect the new component.

React first executes the JavaScript code in ``src/index.js``; this is the entry point for React.  Here is the core code segment of ``src/index.js``: ::


    import React from 'react';
    import ReactDOM from 'react-dom';
    import './index.css';
    import App from './App';

    ReactDOM.render(
        <React.StrictMode>
            <App />
        </React.StrictMode>,
        document.getElementById('root')
    );


It consists of one ``ReactDOM.render`` function call.  There are two parameters in this function call: ``<App />`` which is a React component, and ``document.getElementById('root')`` indicating where to render to.

The first parameter ``<App />`` is imported from ``./App``, which is refering to the ``src/App.js`` file.
As studied in the previous section, ``App.js`` renders a form displaying a value and a button. The form is a `React element`.

The second parameter denotes the destination for rendering, which is a web page on your browser.
Generally, the data structure to represent information on a web page is a 
`Document Object Model (DOM) <https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction>`_, 
a tree-like structure.

React doesn't directly render to the browser DOM yet, instead, it renders into its virtual DOM first, checks the difference between the virtual and browser DOMs, and displays only the changed portions in browser DOM; more efficient this way.

React renders the output of ``<App />``, which is a React element to the DOM element with id as “root”.
React searches for a React element with Id `root` and found this element in ``public/index.html``.  The ``body`` tag of this HTML file is shown here: ::

    <body>
        <noscript>You need to enable JavaScript to run this app.</noscript>
        <div id="root"></div>
    </body>

Take note of the line: ::

    <div id="root"></div>

We call this a `root` DOM node because everything inside it will be managed by React DOM.  This is where all of the React contents go into.  React rendering into the `root` means that everything ``App`` renders will be put into that element.  

As ``ReactDOM.render`` renders the React ``App`` returned element which contains an interactive form, each time a user changes the value in the form, React re-renders ``App`` and places the content in the `root` HTML element to be refreshed on the web page. 
