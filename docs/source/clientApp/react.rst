React Application 🔴
=================

.. note::
    This section requires you to execute commands.  
    
**React** is a declarative, efficient, and flexible JavaScript library for building user interfaces. It’s the ‘V’ in MVC.  ReactJS is an open-source, component-based front end library responsible only for the view layer of the application. It is maintained by Facebook.

React uses a declarative paradigm that makes it easier to reason about your application and aims to be both efficient and flexible. It designs simple views for each state in your application.  React efficiently updates and renders just the right component when your data changes. The declarative view makes your code more predictable and easier to debug.

A **React** application is made up of multiple components, each responsible for rendering a small, reusable piece of HTML. Components can be nested within other components to allow complex applications to be built out of simple building blocks.

We use **web3** to connect the React app with the smart contract and **Ant Design** 
for UI Components. 

Starting ReactApp
----------------

Here is the folder structure of the Simple Storage dApp project: ::

    simple-storage-dapp
    |--client
    |--contracts
    |--docs
    |--migrations
    |--test
    |--truffle-config.js

The ``client`` directory is not a part of the original Truffle project.  It is added to contain program code for a client app using **React**.  However, if you are starting a new client app in a new Truffle project, you 
execute the following command on the terminal in the project root directory ::

    npx create-react-app client

to generate the ``client`` directory in the project root directory and to initialize a new ReactApp.

package.json
============

This file located in the ``client`` directory is a meta data file in any ``node.js`` project.  ::

    {
        "name": "client",
        "version": "0.1.0",
        "private": true,
        "dependencies": {
            "@testing-library/jest-dom": "^5.11.4",
            "@testing-library/react": "^11.1.0",
            "@testing-library/user-event": "^12.1.10",
            "antd": "^4.16.6",
            "react": "^17.0.2",
            "react-dom": "^17.0.2",
            "react-scripts": "4.0.3",
            "web-vitals": "^1.0.1",
            "web3": "^1.4.0"
        },
        "scripts": {
            "start": "react-scripts start",
            "build": "react-scripts build",
            "test": "react-scripts test",
            "eject": "react-scripts eject"
        },
        "eslintConfig": {
            "extends": [
                "react-app",
                "react-app/jest"
            ]
        },
        "browserslist": {
            "production": [
                ">0.2%",
                "not dead",
                "not op_mini all"
            ],
            "development": [
                "last 1 chrome version",
                "last 1 firefox version",
                "last 1 safari version"
            ]
        }
    }

As shown, it records various information and meta-data about your project, which you can change and manage: 

* `version` indicates the current version
* `name` sets the application/package name
* `private` if set to true prevents the app/package to be accidentally published on npm
* `scripts` defines a set of node scripts you can run
* `dependencies` sets a list of npm packages installed as dependencies
* `browserslist` is used to tell which browsers (and their versions) you want to support

For `scripts` listed in the file, they function like command line applications. You can run them by calling ``npm run XXXX` or ``yarn XXXX``, where ``XXXX`` is the command name. Example: ``npm run start``.

The ``eslintConfig`` says that the project is using ``ESLint``, a pluggable linter that tells you if you’ve imported something and not used it, if your function could be short-handed, etc. It will make you a better developer. 


Set Up ReactApp Dependencies 🔴
-----------------------------

The ``SimpleStorage`` project has already added both **web3** and **Ant Design** dependencies in the ``client/package.json`` file that are both needed for the project. :: 

    "dependencies": {
        "@testing-library/jest-dom": "^5.11.4",
        "@testing-library/react": "^11.1.0",
        "@testing-library/user-event": "^12.1.10",
        "antd": "^4.16.6",
        "react": "^17.0.2",
        "react-dom": "^17.0.2",
        "react-scripts": "4.0.3",
        "web-vitals": "^1.0.1",
        "web3": "^1.4.0"
    },

However, the ``node_modules`` files are not included in the ``SimpleStorage`` download from GitLab as these files can be huge.  We need to execute the following command in a new terminal in the ``client`` directory to obtain these dependencies. ::

    npm install 🔴

That is all you need to do at this point in time.

If you're starting a new client app for a new Truffle project (which is not the case here), you need to install these two dependencies as follows.
In a new terminal in the **client** directory, execute the following commands to install 
dependencies for the React App.

To add the **web3** dependency, execute the following command: ::

    npm install web3

You can find more details about **web3** functionalities  
`here <https://web3js.readthedocs.io/en/v1.3.4/index.html>`_.

To install the **Ant Design** dependency, execute the following command. ::

    npm install antd

You can find more on **Ant Design** at `this site <https://ant.design/docs/react/introduce>`_ 
and more about the
**Ant Design Components** `here <https://ant.design/components/overview/>`_.

After you have installed these dependencies for the ReactApp, 
the dependencies are automatically added to the ``package.json`` file in the ``client`` directory as follows.

.. image:: ../images/packageJson.png
    :width: 400

