Add Contract ABI to ReactApp
=============================

.. note::
    There is no code installation or command execution required in this section.  

As mentioned in the :ref:`compile smart contract target` section we will use the contract ABI in React App as follows.
First create ``client/src/abi/`` directory and ``simpleStorageAbi.js`` file in it as follows.   ::

    ethereum-simple-storage
    |--client
    |  |--src
    |  |  |--abi
    |  |  |  |--simpleStorageAbi.js


We copy the following ``abi`` array from the ``SimpleStorage.json`` in ``build`` 
directory in the project root directory. ::

    {
    ...
    "abi": [
        {
            "inputs": [
            {
                "internalType": "uint256",
                "name": "_data",
                "type": "uint256"
            }
            ],
            "stateMutability": "nonpayable",
            "type": "constructor"
        },
        {
            "inputs": [
            {
                "internalType": "uint256",
                "name": "x",
                "type": "uint256"
            }
            ],
            "name": "set",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "get",
            "outputs": [
            {
                "internalType": "uint256",
                "name": "",
                "type": "uint256"
            }
            ],
            "stateMutability": "view",
            "type": "function",
            "constant": true
        }
    ],
    ...
    }

The final version of the ``simpleStorageAbi.js`` script is as follows. ::

	export const simpleStorageAbi = [
		{
		"inputs": [
			{
			"internalType": "uint256",
			"name": "_data",
			"type": "uint256"
			}
		],
		"stateMutability": "nonpayable",
		"type": "constructor"
		},
		{
		"inputs": [
			{
			"internalType": "uint256",
			"name": "x",
			"type": "uint256"
			}
		],
		"name": "set",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
		},
		{
		"inputs": [],
		"name": "get",
		"outputs": [
			{
			"internalType": "uint256",
			"name": "",
			"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function",
		"constant": true
		}
	]

