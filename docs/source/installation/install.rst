Get Tools in Place
==================

.. note::
    This section requires you to execute commands.  
    
The following sections describe how to install all the software tools and dependencies
you need to set up and run this project. You can skip these sections if you have already installed them in your machine.

Install Git
--------------

Git manages code versions for you. 
Use the following command to first install Homebrew in Mac OS. You can find more about Homebrew in
`here <https://brew.sh/>`_ ::

   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

Then use Homebrew to install ``git`` on Mac. ::

   brew install git

This is one particular way to install ``git`` on Mac. More methods 
`here <https://git-scm.com/download/mac>`_.

If you use Windows, you can select and download ``git`` from `this page <https://git-scm.com/downloads>`_. 

Install Node
---------------

You need to install Node Package Manager, or NPM, which comes with Node.js.  
You can see if you have Node already installed in your machine by going to your terminal and typing::

   node -v

If you don't have Node already installed, you can visit the `Node.js website <https://nodejs.org/en/>`_ 
to download it. 
You can find both Mac and Windows versions of Node there. 
It is recommended to use the LTS version of Node.  Then install Node using the downloaded Node installer.

Install Truffle 
------------------

Truffle is a development environment, testing framework, 
and asset pipeline for blockchains using the Ethereum Virtual Machine (EVM). 
Truffle provides important facilities for smart contract development. In this project, 
we use Truffle for smart contract compilation and deploying them to a blockchain. 
You can find more on Truffle at
`this site <https://www.trufflesuite.com/docs/truffle/overview>`_.

You install Truffle globally in your machine using the following command::

   npm install -g truffle

This command can be used to install Truffle in both Mac and Windows.

Install Ganache
-------------------

`Ganache <https://www.trufflesuite.com/ganache>`_  is a UI-based application that runs a blockchain that can be used for development and testing purposes. 
It can be used as your personal blockchain for Ethereum development. 
It is available on Windows, Mac, and Linux as a desktop application and a command line tool.

.. image:: ../images/ganache.png

You can find the latest release for your operating system 
`here <https://github.com/trufflesuite/ganache/releases>`_. 
Once you have downloaded the archived package, extract the installer and run through the setup steps. 
Once you've installed it, you should see following screen when you open it. Choose the **Quickstart** option:

.. image:: ../images/quickstart.png

.. image:: ../images/ganache_accounts.png

**Quickstart** gives you a new locally executing blockchain.

Install MetaMask
-------------------

To interact with a blockchain, we need an interface mechanism. We can use a browser for this purpose.
For the Google Chrome browser, we can install a browser extension called Metamask to turn your web browser into a blockchain browser.

.. image:: ../images/metamask.png

To install Metamask, 
`visit this link <https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en>`_ 
or search for the Metamask Chrome plugin in the Google Chrome web store. 
Once you have installed it, be sure to pin it on the browser.
You will see the Metamask fox icon on the top right part of your Chrome browser when it is installed.

Metamask allows us to manage personal blockchain accounts once we have connected them to the blockchain, 
as well as manage Ether funds that we will need to pay for transactions.
