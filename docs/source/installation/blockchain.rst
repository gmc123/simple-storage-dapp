Get Blockchain Up and Running
====================================

.. note::
    This section requires you to execute commands.  
    
The main Ethereum blockchain network is referred to as the **Mainnet**. 
To use Ethereum **Mainnet**, we need real ``ETH``.  This can be costly.
The good thing is there are test networks that are free for use to test smart contracts: 
**Ropsten**, **Kovan**, **Rinkeby** and **Goerli**. 

For this project, we use Ganache.  
Run Ganache locally on your machine by clicking the Ganache icon.  Choose the **Quickstart** option. You should see the following screen. 

.. image:: ../images/ganache_accounts.png

There are 10 wallet account addresses each having ``100 ETH``, which can be used for testing purposes.
You see the ``MNEMONIC`` list of words above the 10 accounts.
The ``MNEMONIC`` is like a unique identifier for this particular blockchain instance that is now running on your Ganache.  We need this mnemonic when we want to connect or link up the blockchain with wallets, such as MetaMask which we will set up next. 

You also see the ``RPC SERVER`` address ``127.0.0.1:7545``.
It indicates that the blockchain is running locally on port 7545. 
