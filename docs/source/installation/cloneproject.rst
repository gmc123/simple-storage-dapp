Get GitLab Project
===============================

.. note::
    This section requires you to execute commands.  

When you have successfully installed all the aforementioned software tools and dependencies, you can now clone the
"Simple Storage DApp" project from my GitLab account ``gmc123`` to your local computer to look at the source code: ::

   git clone https://gitlab.com/gmc123/simple-storage-dapp.git

Before we learn how to compile and deploy smart contracts, fire up a blockchain, launch the client app, etc., let's get familiar with the project structure and files.

Project Structure
---------------

Here is the folder structure of the Simple Storage dApp project: ::

    simple-storage-dapp
    |--client
    |--contracts
    |--docs
    |--migrations
    |--test
    |--truffle-config.js

Simple Storage dApp is a decentralized application using blockchain as the backend server and React as the frontend client.

* ``client/``: Directory for DApp frontend client files.
* ``contracts/``: Directory for Solidity smart contracts to be deployed in blockchain.
* ``docs/``: Directory for readthedocs Sphinx documnets.
* ``migrations/``: Directory containing smart contract migration scripts; this is used by Truffle.
* ``test/``: Directory for test files for test application and contracts; this is not used.
* ``truffle-config.js``: Truffle configuration file; need to understand what's in this file.

As this dApp is a very simple app, there is only one simple smart contract in the ``contracts/`` directory. Other than this directory, we are concerned mostly with the ``client/`` directory as it contains all the codes and files necessary to provide end users with a client app to interface with a backend blockchain.  This is the frontend/middleware mostly written in Javascript that we want to understand. 

Generally, learning this requires several independent courses: JavaScript Programming, Web Applications, ReactJS Programming, etc.  Clearly, it is not possible to cover all of these in this course.  Here, we use simple code examples to illustrate the essential conceptual components of frontend/middleware.
