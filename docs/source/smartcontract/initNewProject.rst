Starting New Project with Truffle
===============================

.. note::
    There is no code installation or command execution required in this section.  

So far, you have cloned and downloaded the Simple Storage dApp project from GitLab.  On your own, if you wish to start your own blockchain project using Truffle from scratch, run the following command in the root directory to initialize a truffle project: ::

   truffle init

Project Structure
-----------------

Here is the ``simple-storage-dapp`` project structure initialized by Truffle. Your new project structure should also look the same: ::

    simple-storage-dapp
    |--contracts
    |--migrations
    |--test
    |--truffle-config.js

.. note::
    Successful execution of the above command generates all the directories and files except ``client`` and 
    ``docs`` directories.
    We add the ``client`` directory when we need to include a **React App**. 
    All the **Sphinx** documents are placed in the ``docs`` directory.
