Compile and Deploy Smart Contract 🔴
=====================

.. note::
    This section requires you to create code and execute commands.  

We now walk through the process of compiling and deploying smart contract using Truffle. Note that we have already cloned and loaded the Simple Storage dApp files in our computer.

As we are using a local Ganache blockchain running on ``127.0.0.1:7545``, we need to first configure the ``network`` configuration in the ``truffle-config.js`` file.
Open up this file and make sure that 
the network is set to ``127.0.0.1`` and the port to ``7545``.

Solc Version
------------

We define the Solidity compiler version we're gonna use in the ``truffle-config.js`` file.
At the bottom of this file, there is a compiler configuration section. 
We record the exact compiler version in ``solc.version`` as follows. 
In this project we use solidity compiler version ``0.8.1`` . ::

    // Configure your compilers
    compilers: {
        solc: {
            version: "0.8.1",    // Fetch exact version from solc-bin (default: truffle's version)
            ...
        }
    },

If we do not specify the ``solc`` version, it will use Truffle's default ``solc`` version.

Compile Smart Contract
----------------------

In the ``contracts`` directory, there are two contracts:  ``Migrations.sol`` and ``SimpleStorage.sol``.  The former is provided by Truffle when you start a new project.  The latter is the smart contract file coded by us.
Run the following command on a terminal in the project root directory to  
compile all the smart contracts in the ``contracts`` directory.::

   truffle compile

.. note ::
    There are different syntaxes used in different ``solc`` versions.

It compiles successfully if there are no compilation errors in the smart contracts. 
If smart contracts are successfully compiled, you see the following output.

.. image:: ../images/truffleCompile.png

It will generate a ``build`` directory in the root directory. ::

    simple-storage-dapp
    |--build
    |  |--contracts
    |  |  |--Migrations.json
    |  |  |--SimpleStorage.json

Note that Truffle generates a ``SimpleStorage.json`` file in the ``./build/contracts/`` directory each time your compile.  
This file contains lots of information about the ``SimpleStorage`` smart contract, such as ``abi``, ``bytecode``, etc. to be used in React app development.

We need to copy ``SimpleStorage.json`` to the ``./client/src/contracts/`` directory so that the ReactApp knows how to make use of the ``SimpleStorage`` smart contract.
After doing the copying, ``SimpleStorage.json``  is now in two places inside the project folder as follows. ::

    simple-storage-dapp
    |--build
    |  |--contracts
    |  |  |--SimpleStorage.json
    |--client
    |  |--src
    |  |  |--contracts
    |  |  |  |--SimpleStorage.json
    |--contracts
    |--docs
    |--migrations
    |--test
    |--truffle-config.js


Migrate Smart Contract
---------------

Next, we deploy the smart contract into a blockchain network. In Truffle, deployment is called `migration <https://trufflesuite.com/docs/truffle/getting-started/running-migrations.html>`_ (to blockchain).
Truffle provides migration scripts in the ``migrations`` directory. There is an initial migration file in
``migrations`` directory ``1_initial_migration.js``.

We need to add a new migration file ``2_simple_storage_migration.js`` in the ``migrations`` directory for deploying our  **SimpleStorage** smart contract to the blockchain.

In the ``2_simple_storage_migration.js`` script, first we import the ``SimpleStorage`` artifact  as follows. ::

    const SimpleStorage = artifacts.require("SimpleStorage");

Then we deploy the smart contract with constructor arguments. ::

    module.exports = function(deployer) {
        deployer.deploy(<smart_contract_artifact>, <constructor_arguments, ...>);
    };

The final version of the migration file is as follows. We pass ``5`` as the initial value of the ``storedData`` attribute in the **SimpleStorage** smart contract. ::

    const SimpleStorage = artifacts.require("SimpleStorage");

    module.exports = function(deployer) {
        deployer.deploy(SimpleStorage, 5);
    };

Deploy Smart Contract 
----------------------------------

We execute the following command on the terminal in the *project root directory* to 
deploy the **SimpleStorage** 
smart-contract into the Ganache private Etherum blockchain. 
Truffle will deploy smart contracts to Ganache running on network ``127.0.0.1`` port ``7545`` . Before executing the following command, check to make sure that your Ganache Ethereum blockchain is running 🔴 on your machine. ::

    truffle migrate --reset 🔴

We use the ``--reset`` option to run all migrations from the beginning instead of running from the 
last completed migration.

After successfully executing the above command, Truffle prints information about the deployment.
We will use the generated ``contract address`` of the **SimpleStorage** smart contract
in the **ReactApp**.

.. image:: ../images/truffle_migration.png

After successfully deploying the **SimpleStorage** smart contract, Truffle updates the ``SimpleStorage.json`` 
in the ``build/contracts`` directory with the deployed network and the smart contract address.
This file can be used in the **ReactApp**. 
As mentioned above, we need to copy the updated ``SimpleStorage.json`` to the ``./client/src/contracts/`` directory.
